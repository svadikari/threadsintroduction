package com.shyam.threads;

/**
 * Created by svadikari on 2/16/17.
 */

class A {
    synchronized void foo(B b) {
        String name = Thread.currentThread().getName();
        System.out.println(name + " entered A.foo");
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
        }
        System.out.println(name + " trying to call B.last()");
        b.last();
    }

    synchronized void last() {
        System.out.println("Inside A.last");
    }
}

class B {
    synchronized void bar(A a) {
        String name = Thread.currentThread().getName();
        System.out.println(name + " entered B.bar");
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
        }
        System.out.println(name + " trying to call A.last()");
        a.last();
    }

    synchronized void last() {
        System.out.println("Inside B.last");
    }
}


public class Exercise5TheadDeadLock implements Runnable {
    A a = new A();
    B b = new B();

    Exercise5TheadDeadLock() {
        Thread.currentThread().setName("Main Thread");
        new Thread(this).start();
        a.foo(b);
        System.out.println("Back in the main thread.");
    }

    public void run() {
        Thread.currentThread().setName("Racing Thread");
        b.bar(a);
        System.out.println("Back in the other thread");
    }

    public static void main(String args[]) {
        new Exercise5TheadDeadLock();
    }
}


