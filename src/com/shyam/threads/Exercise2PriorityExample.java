package com.shyam.threads;

import java.util.Scanner;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise2PriorityExample {

    static class Thread1 extends Thread {
        public void run() {
            System.out.println(Thread.currentThread().getName()+"::Priority:"+Thread.currentThread().getPriority());
        }
    }
    public static void main(String... args) {
        Thread1 th1 = new Thread1();
        Thread1 th2 = new Thread1();
        th1.setPriority(Thread.MIN_PRIORITY);
        th2.setPriority(Thread.MAX_PRIORITY);
        th1.start();
        th2.start();
    }
}
