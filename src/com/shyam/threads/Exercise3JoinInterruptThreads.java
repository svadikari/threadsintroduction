package com.shyam.threads;

/**
 * Created by svadikari on 2/16/17.
 */

public class Exercise3JoinInterruptThreads {

    static void printMessage(String message) {
        System.out.format("%s: %s%n", Thread.currentThread().getName(),  message);
    }

    private static class MessageLoop implements Runnable {
        public void run() {
            String runningLanes[] = { "Lane 1", "Lane 2", "Lane 3", "Lane 4" };
            try {
                for (int i = 0; i < runningLanes.length; i++) {
                    Thread.sleep(4000);
                    printMessage(runningLanes[i]);
                }
            } catch (InterruptedException e) {
                printMessage("I wasn't done in stipulated time!");
            }
        }
    }

    public static void main(String args[]) throws InterruptedException {
        long patience = 1000 * 8;
        printMessage("Starting the race!!");
        long startTime = System.currentTimeMillis();
        Thread t = new Thread(new MessageLoop());
        t.start();

        printMessage("Waiting for Running race to finish");
        while (t.isAlive()) {
            printMessage("Still waiting...");
            // Wait maximum of 1 second
            // for MessageLoop thread
            // to finish.
            t.join(1000); // w
            if (((System.currentTimeMillis() - startTime) > patience) && t.isAlive()) {
                printMessage("Tired of waiting!");
                t.interrupt();
                // Shouldn't be long now
                // -- wait indefinitely
                t.join();
            }
        }
        printMessage("Ending the race!");
    }
}