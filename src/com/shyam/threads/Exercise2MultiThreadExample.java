package com.shyam.threads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise2MultiThreadExample {


    static class IncrementThread extends Thread {
        int increment=0;
        public void run() {
            while(increment<10) {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"::"+increment++);
            }
        }
    }


    public static void main(String... args) {

        IncrementThread thread1 = new IncrementThread();
        IncrementThread thread2 = new IncrementThread();
        thread1.start();
        thread2.start();
        System.out.println("Please enter your input:::");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input:::"+scanner.nextLine());

    }
}
