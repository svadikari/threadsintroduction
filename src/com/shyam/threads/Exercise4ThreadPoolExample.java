package com.shyam.threads;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise4ThreadPoolExample {

    public static void main(String[] args) throws InterruptedException {
        ThreadPool pool = new ThreadPool(7);
        for (int i = 0; i < 5; i++) {
            Task task = new Task(i);
            System.out.println("<<<<<<<<<<< Executing task: "+i+"  >>>>>>>>>>>>>");
            pool.execute(task);
            Thread.sleep(5000);
        }
    }
}

class Task implements Runnable {

    private int num;
    public Task(int n) {
        num = n;
    }
    public void run() {
        System.out.println("Task " + num + " is running.");
    }
}

class ThreadPool {
    private final LinkedBlockingQueue<Runnable> queue;

    public ThreadPool(int nThreads) {
        queue = new LinkedBlockingQueue<>();
        PoolWorker[] threads = new PoolWorker[nThreads];

        for (int i = 0; i < nThreads; i++) {
            threads[i] = new PoolWorker();
            if(i==6) {
                threads[i].setPriority(Thread.MAX_PRIORITY);
            }
            threads[i].start();
        }
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            System.out.println("I'm notifying..."+Thread.currentThread().getName());
            queue.notify();
        }
    }

    private class PoolWorker extends Thread {
        public void run() {
            Runnable task;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            System.out.println("I'm waiting.."+Thread.currentThread().getName());
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("An error occurred while queue is waiting: " + e.getMessage());
                        }
                    }
                    System.out.println("I'm polling.."+Thread.currentThread().getName());
                    task = queue.poll();
                }

                // If we don't catch RuntimeException,
                // the pool could leak threads
                try {
                    task.run();
                } catch (RuntimeException e) {
                    System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
                }
            }
        }
    }
}
