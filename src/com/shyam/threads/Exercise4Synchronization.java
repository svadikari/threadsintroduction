package com.shyam.threads;

import java.util.Date;

/**
 * Created by svadikari on 2/16/17.
 */
class Document {

    private int pages;
    private String name;

    Document(String name, int pages) {
        this.name = name;
        this.pages = pages;
    }

    public int getPages() {
        return pages;
    }
    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
class Printer {

    void printDocument(Document doc) {
        System.out.println("Start printing...."+doc.getName());
        for (int i = 1; i <= doc.getPages(); i++) {
            System.out.println("printing....page "+i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        System.out.println("Printing completed...."+doc.getName());

    }
}

class MyThread1 extends Thread {
    Printer p;
    MyThread1(Printer p) {
        this.p = p;
    }
    public void run() {
        p.printDocument(new Document("Introduction to Threads", 5));
    }
}

class MyThread2 extends Thread {
    Printer p;
    MyThread2(Printer p) {
        this.p = p;
    }
    public void run() {
        p.printDocument(new Document("Parllel programming using Java",3));
    }
}

public class Exercise4Synchronization {
    public static void main(String args[]) {
        Printer obj = new Printer();
        MyThread1 t1 = new MyThread1(obj);
        MyThread2 t2 = new MyThread2(obj);
        t1.start();
        t2.start();
    }
}
