package com.shyam.threads;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise3JoinThreads {
    public static void main(String[] args) throws InterruptedException{
        Thread th1 = new Thread(new MyClass(), "th1");
        Thread th2 = new Thread(new MyClass(), "th2");
        Thread th3 = new Thread(new MyClass(), "th3");

        th1.start();
        //th1.join(); //Start second thread(th2) once first thread(th1) is dead
        th2.start();
        //th2.join(); //Start second thread(th2) once first thread(th1) is dead
        th3.start();
        //th3.join(); //Start second thread(th2) once first thread(th1) is dead

        System.out.println("All three threads have finished execution");
    }
}

class MyClass implements Runnable{

    @Override
    public void run() {
        Thread t = Thread.currentThread();
        System.out.println("Thread started: "+t.getName());
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println("Thread ended: "+t.getName());

    }
}