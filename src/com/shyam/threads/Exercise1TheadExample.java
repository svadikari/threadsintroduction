package com.shyam.threads;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise1TheadExample extends Thread{

    @Override
    public void run() {
        System.out.println("I'm in Thread example run method....");
    }
    public static void main(String... args) {
        Exercise1TheadExample obj = new Exercise1TheadExample();
        obj.start();
    }
}
