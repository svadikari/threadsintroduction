package com.shyam.threads;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise1RunnableExample implements Runnable{

    @Override
    public void run() {
        System.out.println("I'm in Runnable example run method....");
    }
    public static void main(String... args) {
        Thread obj = new Thread(new Exercise1RunnableExample());
        obj.start();
    }
}
