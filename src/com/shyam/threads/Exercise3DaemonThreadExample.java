package com.shyam.threads;

import java.util.Scanner;

/**
 * Created by svadikari on 2/16/17.
 */
public class Exercise3DaemonThreadExample {

    static class Counter extends Thread {
        int counter = 1;
        public void run() {
            while(counter<=10) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(counter++);
            }
            System.out.println("Exiting from Counter Thread");
        }
    }
    public static void main(String... args) {
        Scanner scanner = new Scanner(System.in);
        Thread counter = new Counter();
        //counter.setDaemon(true);
        counter.start();
        System.out.println("Enter your input data:");
        System.out.println("Input:"+scanner.nextLine());
        System.out.println("Exiting from Main");
    }
}
