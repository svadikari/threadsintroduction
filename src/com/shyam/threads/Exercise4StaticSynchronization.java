package com.shyam.threads;

/**
 * Created by svadikari on 2/16/17.
 */

public class Exercise4StaticSynchronization {

    public static void main(String... args) {
        Thread thread1= new Thread1();
        Thread thread2= new Thread1();
        thread1.start();
        thread2.start();
    }

    static class Thread1 extends Thread{
        public void run() {
            System.out.println(Thread.currentThread().getName() +"::"+Singleton.getInstance().hashCode());
        }
    }

    static class Thread2 extends Thread{
        public void run() {
            System.out.println(Thread.currentThread().getName() +"::"+Singleton.getInstance().hashCode());
        }
    }
}

class Singleton {
    private static volatile Singleton _instance;

    public static Singleton getInstance() {
        if (_instance == null) {
            //synchronized (Singleton.class) {
                if (_instance == null) _instance = new Singleton();
            //}
        }
        return _instance;
    }
}
